CHART_DIR=$1

cd $CHART_DIR
HELM_PACKAGE_NAME=$(docker run --rm -v "${PWD}":/workdir mikefarah/yq:3.4.1 yq r Chart.yaml 'name')
echo "HELM_PACKAGE_NAME: ${HELM_PACKAGE_NAME}"
HELM_PACKAGE_VERSION=$(docker run --rm -v "${PWD}":/workdir mikefarah/yq:3.4.1 yq r Chart.yaml 'version')
echo "HELM_PACKAGE_VERSION: ${HELM_PACKAGE_VERSION}"
helm dependency update .
helm package .
ls -l
CHART_FILENAME=${HELM_PACKAGE_NAME}-${HELM_PACKAGE_VERSION}.tgz
echo "CHART_FILENAME: ${CHART_FILENAME}"
API_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/${CI_COMMIT_BRANCH}/charts
echo "API_URL: ${API_URL}"
curl -v --request POST \
    --user gitlab-ci-token:$CI_JOB_TOKEN \
    --form "chart=@${CHART_FILENAME}" \
    "${API_URL}"